// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.http = axios
Vue.prototype.getDate = getDate

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

function getDate(){
   var date=new Date();

   var year=date.getFullYear() //获取当前年份
   var mon=date.getMonth()+1 //获取当前月份
   var da=date.getDate(); //获取当前日
   var day=date.getDay() //获取当前星期几
   var h=date.getHours() //获取小时
   var m=date.getMinutes() //获取分钟
   var s=date.getSeconds() //获取秒

   return '当前时间:' + year + '年' + mon + '月' + da + '日 ' + h + ':' + m + ':' + s
}
